#!/usr/libexec/platform-python
"""Simple test for APIs used by python3-qrcode
"""
import io
import logging
import hashlib

import qrcode

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()

TEXT = "example data"
HASH = "4d0186bad6cb0ea83f634959bba9bd2494f2b15cc785285e0914521246452e06"


def main():
    qr_output = io.StringIO()
    qr = qrcode.QRCode()
    qr.add_data(TEXT)
    qr.make()
    qr.print_ascii(out=qr_output, tty=False)
    value = qr_output.getvalue()
    print(value)
    assert hashlib.sha256(value.encode('utf-8')).hexdigest() == HASH
    log.info("PASS")


if __name__ == "__main__":
    main()

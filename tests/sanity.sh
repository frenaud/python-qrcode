#!/bin/sh
set -ex
 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
 
dnf install -y python3-qrcode
 
exec ${DIR}/test_ipa_otptoken.py
